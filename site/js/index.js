// récupération des éléments où insérer les chiffres incrémentés
const affairs = document.querySelector("#chiffres .affairs");
const amount = document.querySelector("#chiffres .amount");
const collab = document.querySelector("#chiffres .collab");
//fonction d'incrémenation des chiffres
function animateNumber (number, node, tof) {
	const realNode = node;
	let cpt = 0;
	const timer = setInterval(function () {
		node.innerHTML = cpt.toLocaleString();
		if(number < 5000){
			cpt = cpt+99;
		}else{
			cpt = cpt+999;
		}
		if(cpt >= number){
			node.innerHTML = number.toLocaleString();
			if(tof){
				node.innerHTML += " <span>M€</span>";
			}
			clearInterval(timer);
		}
	}, 90)
}
// actions au chargement de la page
window.onload = function () {
	animateNumber(2617, affairs, true);
	animateNumber(26820 , amount, true);
	animateNumber(3440, collab, false);

	slideTime ();

	if(window.innerWidth >= 768){
		slides[0].setAttribute("class", "d-grid fade");
		slides[1].setAttribute("class", "d-grid fade d-none");
		slides[2].setAttribute("class", "d-grid fade d-none");
	}
};
// recupération des élements de slides (contenus et blocs cliquables)
const slides = document.querySelectorAll("#chapitres section");
const blocs = document.querySelectorAll("#chapitres .chap-blocs>div");
// écouteurs d'évenements sur les blocs cliquables
blocs.forEach( function(element, index) {
	element.addEventListener("click", slideClick, false);
});
// actition au clic sur un des blocs
function slideClick (arg) {
	let argPos;
	blocs.forEach( function(element, index) {
		if(arg.currentTarget == element){
			argPos = index;
		}
	});
	slides.forEach( function(element, index) {
		if (argPos == index) {
			element.setAttribute("class", "d-grid fade");
		} else {
			element.setAttribute("class", "d-grid fade d-none");
		}
	});
}
// fonction changement du slide sur timer
function slideTime () {
	let argPos = 0;
	const timer = setInterval(function () {
		if(argPos>2) argPos = 0;
		if(window.innerWidth >= 768){
			slides.forEach( function(element, index) {
				if (argPos == index) {
					element.setAttribute("class", "d-grid fade");
				} else {
					element.setAttribute("class", "d-grid fade d-none");
				}
			});
			argPos++;
		}
		
	}, 10000)
}
// cacher les blocs de slides au redimensionnement de l'écran
window.onresize = function(){
	if(window.innerWidth >= 768){
		slides[0].setAttribute("class", "d-grid fade");
		slides[1].setAttribute("class", "d-grid fade d-none");
		slides[2].setAttribute("class", "d-grid fade d-none");
	}else{
		slides[0].setAttribute("class", "d-grid fade");
		slides[1].setAttribute("class", "d-grid fade");
		slides[2].setAttribute("class", "d-grid fade");
	}
}
